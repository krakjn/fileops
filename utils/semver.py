import re
import subprocess


OFFICIAL_SEMVER_REGEX = re.compile(
    r"^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"
)


def get_sha() -> str:
    """return 7 character SHA of current commit"""
    return (
        subprocess.run(["sh", "-c", "git rev-parse --short HEAD"], capture_output=True)
        .stdout.decode("utf8")
        .strip("\n")
    )


# def get_latest_tag() -> str:
#     """
#     returns last known tag that is an ancestor of this commit
#     otherwise returns "fatal: ..."
#     """
#     description = subprocess.run(["sh", "-c" "git describe"], capture_output=True)
#     return description.stdout.decode("utf8").strip("\n")


def get_desc() -> str:
    """
    if current commit is a tag return the tag string
    else return fatal: No annotated tags can describe 'sha...'
    """
    return (
        subprocess.run(
            ["sh", "-c", "git describe --exact-match 2> / dev/null"],
            capture_output=True,
        )
        .stdout.decode("utf8")
        .strip("\n")
    )


def semver() -> str:
    """
    Retrieve 7 char SHA or semantic version tag depending on current git commit
    """
    desc = get_desc()
    if re.match(OFFICIAL_SEMVER_REGEX, desc):
        return desc
    else:
        return get_sha()


if __name__ == "__main__":
    print(semver())
