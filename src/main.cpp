#include "CLI/CLI.hpp"
#include "fileops.h"
#include <iostream>
#include <string>

/**
 * @brief To handle output of fileops calls and notify user of status
 *
 * @param result The result of a completed fileops call
 */
void handle_result(fileops::Result const &result) {
    using namespace fileops;
    switch (result.get()) {
        case Error::NONE: printf("operation completed sucessfully\n"); break;
        case Error::INVALID_NAME: printf("error: invalid character\n"); break;
        case Error::NOT_ENOUGH_PERMISSION: printf("not enough permissions\n"); break;
        case Error::NOT_FOUND: printf("file not found\n"); break;
        case Error::BADBIT: printf("read/writing error on i/o operation\n"); break;
        case Error::EOFBIT: printf("end-of-file reached on input operation\n"); break;
        case Error::FAILBIT: printf("logical error on i/o operation, incorrect filename?\n"); break;
        case Error::MULTIBIT: printf("multiple error bits set\n"); break;
    }
}

int main(int argc, char **argv) {

    CLI::App app("fileops, common file operations");
    app.set_help_all_flag("--help-all", "Expand all help");

    CLI::App *create = app.add_subcommand("create", "Create file, warning will overwrite");
    std::string filename;
    std::vector<std::string> text; // since vectored argument multiple arguments
    bool joined = false;
    create->add_option("filename", filename, "Name of file to create");
    create->add_flag("-j,--join", joined, "Join all text into single line");
    CLI::Option *text_opt = create->add_option("-t,--text", text, "Text to add to file");
    create->callback([&filename, &text_opt, &text, &joined]() -> void {
        if (*text_opt) {
            handle_result(fileops::create(filename, text, joined));
        } else {
            handle_result(fileops::create(filename));
        }
    });

    CLI::App *copy = app.add_subcommand("copy", "copy a file into another");
    std::string src;
    std::string dest;
    copy->add_option("src", src, "source");
    copy->add_option("dest", dest, "destination");
    copy->callback([&src, &dest]() -> void {
        handle_result(fileops::copy(src, dest));
    });

    CLI::App *concat = app.add_subcommand("concat", "Append two files into a third, order matters!");
    std::string file1;
    std::string file2;
    std::string output;
    concat->add_option("file1", file1, "First file");
    concat->add_option("file2", file2, "Second file");
    concat->add_option("output", output, "Output file");
    concat->callback([&file1, &file2, &output]() -> void {
        handle_result(fileops::concat(file1, file2, output));
    });

    CLI::App *del = app.add_subcommand("del", "delete a file");
    std::string delete_file;
    del->add_option("filename", delete_file, "Name of file to create");
    del->callback([&delete_file]() -> void {
        handle_result(fileops::del(delete_file));
    });

    CLI11_PARSE(app, argc, argv);

    if (argc == 1) {
        printf("no command given, try \"--help\"\n");
    }

    return 0;
}
