#ifndef FILEOPS_H
#define FILEOPS_H

#include <string>
#include <vector>

namespace fileops {

/**
 * @brief Errors of known file operations
 *
 */
enum class Error {
    NONE,
    INVALID_NAME,
    NOT_ENOUGH_PERMISSION,
    NOT_FOUND,
    EOFBIT,
    FAILBIT,
    BADBIT,
    MULTIBIT
};

/**
 * @brief type to hold the result of file operations
 *
 */
class Result {
  private:
    Error err;

  public:
    // fileops shouldn't care about printing
    Result() : err{Error::NONE} {}
    Result(Error e) : err{e} {}
    Error get() const { return err; }
    operator bool() { return err == Error::NONE ? true : false; }
    Result operator=(Error err) {
        this->err = err;
        return *this;
    }
};

/**
 * @brief to create file on disk
 *
 * @param filename name of file
 * @return Result enum representing file state
 */
Result create(std::string const &filename);

/**
 * @brief to create file on disk with text
 *
 * @param filename name of file
 * @param args a vector of text to append to file
 * @param joined whether to join all on the same line
 * @return Result enum representing file state
 */
Result create(std::string const &filename, std::vector<std::string> &args, bool joined = false);

/**
 * @brief copy file from source to destination
 *
 * @param src source file to copy from
 * @param dest destination file to copy to
 * @return Result enum representing file state
 */
Result copy(std::string const &src, std::string const &dest);

/**
 * @brief take two files and append them into a third file
 *
 * @param file1 first source file
 * @param file2 second source file
 * @param output output file
 * @return Result enum representing file state
 */
Result concat(std::string const &file1, std::string const &file2, std::string const &output);

/**
 * @brief deletes file on disk
 *
 * @param filename name of file to delete
 * @return Result enum representing file state
 */
Result del(std::string const &filename);

/**
 * @brief Get the size object. This function is useful to test
 *        other functions like concat
 *
 * @param filename name of file
 * @return uint16_t size in bytes of file
 */
uint16_t get_size(std::string const &filename);

} // namespace fileops
#endif // FILEOPS_H