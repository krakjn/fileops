#include "fileops.h"
#include <fstream>
#include <iostream>
#include <stdint.h>
#include <typeinfo>

namespace fileops {

uint16_t get_size(std::string const &filename) {
    uint16_t size = 0;
    std::fstream fs(filename, std::fstream::in | std::fstream::ate);
    size = fs.tellg();
    fs.close();
    return size;
}

template <typename... Fstreams> Result ok(Fstreams... fstreams) {
    Result result;
    for (std::fstream *fs : {fstreams...}) {
        if (!fs->is_open()) {
            result = Error::NOT_FOUND;
            break;
        }
        if (!fs->good()) {
            switch (fs->rdstate()) {
                case std::ios::badbit: result = Error::BADBIT; break;
                case std::ios::eofbit: result = Error::EOFBIT; break;
                case std::ios::failbit: result = Error::FAILBIT; break;
                default: result = Error::MULTIBIT; break;
            }
            break;
        }
    }
    return result;
}

Result create(std::string const &filename) {
    std::fstream fs(filename, std::fstream::out | std::fstream::trunc);
    Result result = ok<std::fstream *>(&fs);
    fs.close(); // flushes buffer onto disk
    return result;
}

Result create(std::string const &filename, std::vector<std::string> &text, bool joined) {
    std::fstream fs(filename, std::fstream::out | std::fstream::trunc);
    Result result = ok<std::fstream *>(&fs);
    if (result) {
        for (std::string line : text) {
            if (joined) {
                fs << line << " ";
            } else {
                fs << line << "\n";
            }
        }
        if (joined) {
            long pos = fs.tellp();
            fs.seekp(pos - 1); // remove extra space at the end
            fs.write("\n", 1); // *nix file systems like termination
        }
    }
    fs.close();
    return result;
}

Result copy(std::string const &src, std::string const &dest) {
    std::fstream srcfs(src, std::fstream::in);
    std::fstream destfs(dest, std::fstream::out | std::fstream::trunc);
    Result result = ok<std::fstream *>(&srcfs, &destfs);
    if (result) {
        destfs << srcfs.rdbuf();
    }
    srcfs.close();
    destfs.close();
    return result;
}

Result concat(std::string const &file1, std::string const &file2, std::string const &output) {
    std::fstream one_fs(file1, std::fstream::in);
    std::fstream two_fs(file2, std::fstream::in);
    std::fstream out_fs(output, std::fstream::out | std::fstream::trunc);

    Result result = ok<std::fstream *>(&one_fs, &two_fs, &out_fs);
    if (result) {
        out_fs << one_fs.rdbuf();
        out_fs << two_fs.rdbuf();
    }
    one_fs.close();
    two_fs.close();
    out_fs.close();
    return result;
}

Result del(std::string const &filename) {
    Result result;
    for (char c : filename) {
        if (c == '/') {
            result = Error::INVALID_NAME;
        }
    }

    if (result) { // valid name

        if (std::remove(filename.c_str()) != 0) {
            result = Error::NOT_FOUND;

        } else { // was removed
            std::fstream fs(filename, std::fstream::in);
            if (ok<std::fstream *>(&fs)) { // good and open
                result = Error::NOT_ENOUGH_PERMISSION;
            }
        }
    }
    return result;
}

} // namespace fileops