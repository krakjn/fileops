FROM debian:bookworm-slim
RUN DEBIAN_FRONTEND=noninteractive apt update && apt install -y \
    g++ cmake ninja-build python3 sudo wget gpg

# think of every comma as hitting enter
RUN ["/bin/bash", "-c", "set -o pipefail \
    && wget -qO - 'https://proget.makedeb.org/debian-feeds/makedeb.pub' \
    | gpg --dearmor | tee /usr/share/keyrings/makedeb-archive-keyring.gpg 1> /dev/null \
    && echo 'deb [signed-by=/usr/share/keyrings/makedeb-archive-keyring.gpg arch=all] https://proget.makedeb.org/ makedeb main' \
    | tee /etc/apt/sources.list.d/makedeb.list" ]
RUN DEBIAN_FRONTEND=noninteractive apt update && apt install -y makedeb

RUN useradd --system --shell /bin/bash ci
# # to make account passwordless
RUN passwd -d ci
RUN echo 'ci ALL=(ALL:ALL) NOPASSWD: ALL' >> /etc/sudoers
WORKDIR /ci
USER ci
CMD ["bash"]
