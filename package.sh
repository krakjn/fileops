#!/bin/bash
DATE=$(date -R)
VERSION="0.1.0"

mkdir -p fileops${VERSION}/DEBIAN 
mkdir -p fileops${VERSION}/usr/bin 
mkdir -p fileops${VERSION}/usr/lib

cat << EOF > fileops${VERSION}/DEBIAN/changelog 
fileops (0.1.0) unstable; urgency=low

  * Initial release

 -- Tony B <krakjn@gmail.com>  ${DATE} 
EOF

cat << EOF > fileops${VERSION}/DEBIAN/control
Package: fileops
Version: 0.1.0
Architecture: amd64
Maintainer: Tony B <krakjn@gmail.com>
Description: Common File Operations tool
Depends: libc6 (>= 2.32)
EOF

cp build/dist/cli fileops${VERSION}/usr/bin/fileops
cp build/dist/libfileops.so fileops${VERSION}/usr/lib/libfileops.so

dpkg-deb --build fileops${VERSION}
# dpkg -i fileops${VERSION}
