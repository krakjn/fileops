# build with CMake
build:
    @cmake -B build/ -G "Ninja"
    @cmake --build build/
    @ctest --test-dir build/

# out of source build
clean:
    @rm -rf build/

run *args:
    @./build/dist/cli {{args}}
