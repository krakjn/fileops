#include <gtest/gtest.h>
#include "fileops.h"

using namespace fileops;

TEST(valid, create_file) {
  EXPECT_TRUE((bool)create("file"));
}

TEST(valid, create_file_with_text) {
  std::vector<std::string> text = {"alrighty\tthen!", "Somebody...\nstop me!"};
  EXPECT_TRUE((bool)create("filewithtext", text));
}

TEST(valid, create_file_with_joined_text) {
  std::vector<std::string> text = {"roses", "are", "red"};
  EXPECT_TRUE((bool)create("filewithjoinedtext", text, true));
}

TEST(valid, copy_file_with_text) {
  EXPECT_TRUE((bool)copy("filewithtext", "filewithtext_copy"));
  uint16_t file1_size = get_size("filewithtext");
  uint16_t file2_size = get_size("filewithtext_copy");
  EXPECT_EQ(file1_size, file2_size);
}

TEST(valid, concatenation) {
  EXPECT_TRUE((bool)concat("filewithtext", "filewithjoinedtext", "fileconcat"));
  uint16_t file1_size = get_size("filewithtext");
  uint16_t file2_size = get_size("filewithjoinedtext");
  uint16_t file3_size = get_size("fileconcat");
  EXPECT_EQ(file1_size + file2_size, file3_size);
}

TEST(valid, deletion) {
  EXPECT_TRUE((bool)del("file"));
}

// invalid char '/' in unix filename 

TEST(invalid, create_file) {
  EXPECT_FALSE((bool)create("fi/le"));
}

TEST(invalid, create_file_with_text) {
  std::vector<std::string> text = {"alrighty\tthen!", "Somebody...\nstop me!"};
  EXPECT_FALSE((bool)create("filewith/text", text));
}

TEST(invalid, create_file_with_joined_text) {
  std::vector<std::string> text = {"roses", "are", "red"};
  EXPECT_FALSE((bool)create("filewith/joinedtext", text, true));
}

TEST(invalid, copy_file_with_text) {
  EXPECT_FALSE((bool)copy("filewith/text", "filewithtext_copy"));
  EXPECT_FALSE((bool)copy("filewithtext", "filewith/text_copy"));
  EXPECT_FALSE((bool)copy("filewith/text", "filewith/text_copy"));
}

TEST(invalid, concatenation) {
  EXPECT_FALSE((bool)concat("filewith/text", "filewithjoinedtext", "fileconcat"));
  EXPECT_FALSE((bool)concat("filewithtext", "filewith/joinedtext", "fileconcat"));
  EXPECT_FALSE((bool)concat("filewithtext", "filewithjoinedtext", "file/concat"));
  EXPECT_FALSE((bool)concat("filewith/text", "filewith/joinedtext", "file/concat"));
}

TEST(invalid, deletion) {
  EXPECT_FALSE((bool)del("fi/le"));
}

// nonexistent files 

TEST(nonexistent, copy_file_with_text) {
  EXPECT_FALSE((bool)copy("nonexist", "impossible"));
}

TEST(nonexistent, concatenation) {
  EXPECT_FALSE((bool)concat("nonexist1", "nonexist2", "impossible_concat"));
}

TEST(nonexistent, deletion) {
  EXPECT_FALSE((bool)del("unknown"));
}
