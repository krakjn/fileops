# DevOps Code Challenge
Write a file manipulation CLI tool using C++11
or later. Provide commands for the following
operations:

- Create a file (empty or with optional text)
- Copy a file
- Combine two files into a third
- Delete a file

Your proiect should be split into a C++ shared
library and executable. Include unit/integration
tests, documentation, and anything else you
feel is necessary for a well-maintained project.
You must use a third-party library at some point
in your project (we recommend CLI11 for
parsing arguments, but feel free to use
whatever you're comfortable with).
Most importantly, write a script that mimics a Cl
pipeline and produces a versioned Debian
package. Your script should execute
successfully on a fresh instance of Debian-
based Linux.
